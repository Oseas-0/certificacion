package com.personal.controladores;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.entidades.Maquina;
import com.personal.entidades.Maquina.Tipo;
import com.personal.repositorios.IMaquinaRepository;

@Controller
@CrossOrigin
@RequestMapping("maquina")
public class MaquinaController {

	/*Metodo Para Retornar La Vista*/
	@GetMapping("index")
	public String Index() {
		return "Views/Maquina/maquina";
	}

	/*Accediendo A Capa De Datos*/
	@Autowired IMaquinaRepository rMaquina;
	

	/*Metodo Para Retornar Un Listado De Registros*/
	@GetMapping(value = "lista", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Maquina> Listar() {
		return (List<Maquina>) rMaquina.findAll();
	}
	
	/*Metodo Para Guardar Registro*/
	@GetMapping("guardar")
	@ResponseBody
	public HashMap<String, String> Guardar(@RequestParam String nombre, @RequestParam String descripcion, @RequestParam Tipo tipo) {

		Maquina m = new Maquina();

		HashMap<String, String> jsonReturn = new HashMap<>();
		m.setNombre(nombre);
		m.setDescripcion(descripcion);
		m.setTipo(tipo);

		try {
			rMaquina.save(m);
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro Guardado");

			return jsonReturn;
		} catch (Exception ex) {
			jsonReturn.put("Estado", "ERROR");
			jsonReturn.put("Mensaje", "Registro No Guardado" + ex.getMessage());
			return jsonReturn;
		}
	}
	
	/*Metodo Para Cargar Datos En Inputs Por Medio Del Id Del Registro*/
	@GetMapping(value = "cargar/{codigo}")
	@ResponseBody
	public Maquina Cargar(@PathVariable Integer codigo) {
	    return rMaquina.findById(codigo).get();
	}
	
	/*Metodo Para Guardar La Modificacion O Actualizacion De Un Registro*/
	@GetMapping("modificar")
	@ResponseBody
	public HashMap<String, String> Actualizar(@RequestParam Integer codigo, @RequestParam String nombre, @RequestParam String descripcion, @RequestParam Tipo tipo) {

		Maquina m=new Maquina();
				 
		HashMap<String, String> jsonReturn=new HashMap<>();
		m.setCodigo(codigo);
		m.setNombre(nombre);
		m.setDescripcion(descripcion);
		m.setTipo(tipo);
				 
		try {
			rMaquina.save(m);
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro Actualizado");			 
			return jsonReturn;
					 
		}catch(Exception ex) {
			jsonReturn.put("Estado", "ERROR");
			jsonReturn.put("Mensaje", "Registro No Actualizado"+ex.getMessage());
			return jsonReturn;
		}
				 
	}
	
	/*Metodo Para Eliminar Un Registro Por Medio Del Id*/
	@GetMapping(value="eliminar/{codigo}")
	@ResponseBody
	public HashMap<String,String> Eliminar (@PathVariable Integer codigo) {
			 
		HashMap<String, String> jsonReturn=new HashMap<>();
		        
		try {
			Maquina m = rMaquina.findById(codigo).get();
			rMaquina.delete(m);
			jsonReturn.put("Estado", "OK");
			jsonReturn.put("Mensaje", "Registro Eliminado");
			return jsonReturn;
		        	
		}catch(Exception ex) {
			jsonReturn.put("Estado", "ERROR");
			jsonReturn.put("Mensaje", "Registro No Eliminado"+ex.getMessage());
			return jsonReturn;
		}
	}
}
