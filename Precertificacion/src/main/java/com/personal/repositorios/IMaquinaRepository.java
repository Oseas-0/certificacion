package com.personal.repositorios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.entidades.Maquina;

@Repository
public interface IMaquinaRepository extends CrudRepository<Maquina, Integer>{

}
