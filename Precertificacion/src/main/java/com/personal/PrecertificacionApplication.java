package com.personal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrecertificacionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrecertificacionApplication.class, args);
	}

}
