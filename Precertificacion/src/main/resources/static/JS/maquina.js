let maquina = {
	codigo: 0,
}

/* Ejecucion De La Funcion Inicio Cuando El Documento HTML Este Cargado */
$(document).ready(inicio);

/* Funcion INICIO Donde Se Inicializan Los Funciones Para Guardar, Listar y Modificar */
function inicio() {
	ListaDatos();
	$("#btnGuardar").click(validar);
	$("#btnModificar").click(Actualizar);
	$("#btncancelar").click(Reset);
}

/* Funcion RESET Para Limpiar Los Inputs */
function Reset() {
	$("#nombre").val(null);
	$("#descripcion").val(null);
	$("#tipo").prop('selectedIndex',0);
}

/* Funcion Para Listar Los Datos En La Tabla */
function ListaDatos() {
	$.ajax({
			url: "/maquina/lista",
			method: "Get",
			success: function (response) {

				$("#datos").html("");

				response.forEach(i => {

					$("#datos")
						.append(""
							+ "<tr>"
							+ "<td>" + i.codigo + "</td>"
							+ "<td>" + i.nombre + "</td>"
							+ "<td>" + i.descripcion + "</td>"
							+ "<td>" + i.tipo + "</td>"
							+ "<td>" + "<button type='button' onclick='CargarInputs(" + i.codigo + ");' class='btn btn-warning' data-toggle='modal' data-target='#Editar'>Editar</button>"
							+ "</td>"
							+ "<td>"
							+ "<button type='button' onclick='ConfirmEliminar(" + i.codigo + ");' class='btn btn-danger'>Eliminar</button>"
							+ "</td>"
							+ "</tr>"
						);
				});
			},
			error: function (response) {
				alert("ERROR " + response);
			}
		});
}

/* Funcion Para Guardar */
function Guardar() {
	$.ajax({
		url: "/maquina/guardar",
		method: "Get",
		data: {
			nombre: $("#nombre").val(),
			descripcion: $("#descripcion").val(),
			tipo: $("#tipo").val(),
		},
		success: function (response) {
			AlertGuardar();
			ListaDatos();
			Reset();
		},
		error: function (response) {
			alert("error al guardar " + response);
		}
	})
}

/* Alerta de Confirmacion Cuando Un Registro Sea Guardado */
function AlertGuardar() {
	Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Guardado Exitosamente',
		showConfirmButton: false,
		timer: 1500
	})
}

/* Metodo Para Cargar Los Datos En Los Inputs */
function CargarInputs(codigo) {
	$.ajax({
		url: "/maquina/cargar/" + codigo,
		method: "Get",
		success: function (response) {
			$("#codigo").val(response.codigo);
			$("#nombre2").val(response.nombre);
			$("#descripcion2").val(response.descripcion);
			$("#tipo2").val(response.tipo);
		},
		error: function (response) {
			alert("error al cargar inputs " + response);
		}
	})
}

/* Metodo Para Actualizar Un Registro */
function Actualizar() {
	$.ajax({
		url: "/maquina/modificar",
		method: "Get",
		data: {
			codigo: $("#codigo").val(),
			nombre: $("#nombre2").val(),
			descripcion: $("#descripcion2").val(),
			tipo: $("#tipo2").val(),
		},
		success: function (response) {
			AlertModificar();
			ListaDatos();
		},
		error: function (response) {
			alert("error al modificar " + response);
		}
	})
}

/* Alerta De Confirmacion Cuando Un Registro Sea Modificado */
function AlertModificar() {
	Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Actualizado Exitosamente',
		showConfirmButton: false,
		timer: 1500
	})
}

/* Metodo Para Eliminar Registros */
function Eliminar(codigo) {
	$.ajax({
		url: "/maquina/eliminar/" + codigo,
		method: "Get",
		success: function (response) {
			AlertEliminar();
			ListaDatos();
		},
		error: function (response) {
			alert("error al eliminar " + response);
		}
	})
}

/* Alerta De Verificacion, Si El Usuario Esta Seguro De Eliminar El Registro*/
function ConfirmEliminar(codigo) {
	Swal.fire({
		title: 'Eliminar',
		text: "¿Estas seguro de elimiar el registro?",
		icon: 'warning',
		showCancelButton: true,
		cancelButtonColor: '#3085d6',
		confirmButtonColor: '#d33',
		cancelButtonText:'No',
		confirmButtonText: 'Si'
	}).then((result) => {
		if (result.value) {
			Eliminar(codigo);
		}
	})
}

/* Alerta De Confirmacion Cuando Un Registro Sea Eliminado */
function AlertEliminar(){
	Swal.fire({
		position: 'center',
		icon: 'success',
		title: 'Eliminado!',
		text: 'El registro fue Eliminado correctamente',
		showConfirmButton: false,
		timer: 1500
	});
}

/*Funcion Para No Guardar Campos Vacios*/
function validar() {
	var nombre = $("#nombre").val().trim();
	var descripcion = $("#descripcion").val().trim();
	
	if (nombre.length == 0) {
		$("#mensaje").fadeIn();
		return false;
	}else if(descripcion.length == 0){
		$("#mensaje2").fadeIn();
		return false;
	}else{
		Guardar();
	}
}